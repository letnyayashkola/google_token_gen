import os
import pickle

from google_auth_oauthlib.flow import InstalledAppFlow


SCOPES = ["https://www.googleapis.com/auth/spreadsheets"]
CRED_FILE = "credentials.json"
BUILD_DIR = "build"
TOKEN_FILE = "token.pickle"


def main():
    base_dir = os.getcwd()

    flow = InstalledAppFlow.from_client_secrets_file(
        os.path.join(base_dir, CRED_FILE), SCOPES
    )

    creds = flow.run_local_server()

    dir_ = os.path.join(base_dir, BUILD_DIR)

    if not os.path.exists(dir_):
        os.makedirs(dir_)

    token_file = os.path.join(dir_, TOKEN_FILE)

    with open(token_file, "wb") as token:
        pickle.dump(creds, token)


if __name__ == "__main__":
    main()
