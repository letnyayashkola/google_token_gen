# Info

Проект нужен для получения токенов к Google Sheet API.

Вместе с исходниками надо положить файл `credentials.json`.

[Google Quickstart](https://developers.google.com/sheets/api/quickstart/python)
